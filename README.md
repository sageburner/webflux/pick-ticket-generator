# Pick Ticket Generator (Flux)

Generate a constant stream of Pick Ticket requests via REST

![Pick-Ticket Architecture](pick-ticket.png "Pick-Ticket Architecture")

## Build & Run


### Run Locally (Boot Run)

Environment Variables

```
STREAM_DELAY_MIN=1 <- minimum time to wait between sending REST requests
STREAM_DELAY_MAX=60 <- maximum time to wait between sending REST requests
TARGET_REST_URL=http://localhost:8080/pickticket <- REST endpoint for the Pick Ticket Consumer
```

Run Service

`./gradlew bootRun`

### Run with Docker

Build the Docker image

`./gradlew buildDocker`

Build and Publish the Docker Image

`./gradlew buildDocker -Ppublish=true`

Run a Docker container

`docker run --rm --network=host --name=pick-ticket-generator registry.gitlab.com/sageburner/webflux/pick-ticket-generator:0.1.0`
