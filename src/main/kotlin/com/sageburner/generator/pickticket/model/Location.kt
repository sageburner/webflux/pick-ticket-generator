package com.sageburner.generator.pickticket.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID

/**
 * Location
 *
 */
data class Location(
        @JsonProperty("locationId") val id: UUID,
        @JsonProperty("locationName") val name: String,
        @get:JsonIgnore val isSupplying: Boolean)
