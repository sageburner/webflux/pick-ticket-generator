package com.sageburner.generator.pickticket.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

/**
 * RequisitionLine
 *
 */
data class RequisitionLine(
        val id: UUID,
        @JsonProperty("qtyNeeded") val quantityNeeded: String,
        val parId: UUID)
