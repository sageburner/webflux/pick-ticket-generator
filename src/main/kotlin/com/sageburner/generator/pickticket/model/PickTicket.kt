package com.sageburner.generator.pickticket.model

import java.util.UUID

/**
 * PickTicket
 *
 */
data class PickTicket(
        val id: UUID,
        val supplyingLocation: Location,
        val requestingLocation: Location,
        val note: String = "note",
        val caseNumber: String?,
        val requisitionLines: List<RequisitionLine>)
