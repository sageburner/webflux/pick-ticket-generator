package com.sageburner.generator.pickticket

import com.sageburner.generator.pickticket.service.PickTicketService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.PropertySource
import org.springframework.core.env.Environment
import org.springframework.retry.RetryCallback
import org.springframework.retry.support.RetryTemplate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

/**
 * PickTicketGenerator application
 *
 * Generate a continuous stream of pick ticket requests
 *
 */
@SpringBootApplication
@PropertySource("classpath:application.properties")
class PickTicketGenerator : InitializingBean, DisposableBean {

    private val log = LoggerFactory.getLogger(PickTicketGenerator::class.java)

    @Autowired
    private lateinit var env: Environment

    @Autowired
    lateinit var retryTemplate: RetryTemplate

    @Autowired
    lateinit var pickTicketService: PickTicketService

    override fun afterPropertiesSet() {
        log.info("Application Version: " + env.getProperty("app.version"))
        configureSSLContext()
        startFeeding()
    }

    override fun destroy() {
        log.info("Destroying ...")
    }

    fun startFeeding() {
        try {
            retryTemplate.execute(RetryCallback<Unit, Exception> { pickTicketService.start() })
        } catch (e: Exception) {
            log.error("RetryTemplate Error!!!")
            e.printStackTrace()
        }
    }

    private fun configureSSLContext() {
        try {
            val ctx = SSLContext.getInstance("TLS")
            val tm = object : X509TrustManager {
                override fun checkClientTrusted(p0: Array<out java.security.cert.X509Certificate>, p1: String) { }

                override fun checkServerTrusted(p0: Array<out java.security.cert.X509Certificate>, p1: String) { }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return emptyArray()
                }
            }

            ctx.init(null, arrayOf<TrustManager>(tm), null)

            SSLContext.setDefault(ctx)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }
}

fun main(args: Array<String>) {
    SpringApplication.run(PickTicketGenerator::class.java, *args)
}
