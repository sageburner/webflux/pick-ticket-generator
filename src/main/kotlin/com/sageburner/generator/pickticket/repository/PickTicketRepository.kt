package com.sageburner.generator.pickticket.repository

import com.sageburner.generator.pickticket.model.Location
import com.sageburner.generator.pickticket.model.PickTicket
import com.sageburner.generator.pickticket.model.RequisitionLine
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.SynchronousSink
import java.security.SecureRandom
import java.time.Duration
import java.util.*

@Component
class PickTicketRepository : InitializingBean, DisposableBean {

    private val log = LoggerFactory.getLogger(PickTicketRepository::class.java)

    @Autowired
    private lateinit var env: Environment

    lateinit var pickTicketFlux: Flux<PickTicket>

    override fun afterPropertiesSet() {
        log.info("Initializing...")
        buildCache()
        pickTicketFlux = generatePickTicketStream(Duration.ofMillis(env.getProperty("app.stream.pickticket.interval.ms", "250").toLong() ))
    }

    override fun destroy() {
        log.info("Destroying ...")
    }

    private val locations: MutableList<Location> = mutableListOf()
    private val locationsById: MutableMap<UUID, Location> = mutableMapOf()
    private val notes: MutableList<String> = mutableListOf()
    private val requestingLocationByPar: MutableMap<UUID, UUID> = mutableMapOf()
    private val supplyingLocationByPar: MutableMap<UUID, UUID> = mutableMapOf()
    private lateinit var parsByRequestingLocation: Map<UUID, List<UUID>>
    private lateinit var parsBySupplyingLocation: Map<UUID, List<UUID>>

    fun generatePickTicketStream(period: Duration): Flux<PickTicket> {
        return Flux.generate({ 0 } , {
            index, sink: SynchronousSink<PickTicket> ->
                val pickTicket = createPickTicket()
                sink.next(pickTicket)
                index + 1
            })
    }


    fun createPickTicket(): PickTicket {

        // PickTickets
        /**
         * 1. Find pars from random Supplying Location
         * 2. Find pars from random Requesting Location
         * 3. Intersect sets
         * 4. Empty set?
         *      yes -> Start over at 1
         *      no  -> continue
         * 5. Take random selection of pars
         * 6. Add pars to PickTicket
         */
        var requisitionPars: Set<UUID>
        do {
            // 1.
            val parsAtSupplyingLocation: List<UUID> = getParsFromRandomSupplyingLocation()
            // 2.
            val parsAtRequestingLocation: List<UUID> = getParsFromRandomRequestingLocation()
            // 3.
            requisitionPars = parsAtSupplyingLocation.intersect(parsAtRequestingLocation)
        } while (requisitionPars.isEmpty())

        val parId: UUID = requisitionPars.first()
        val supplyingLocationId: UUID = supplyingLocationByPar[parId] ?: UUID.fromString("")
        val requestingLocationId: UUID = requestingLocationByPar[parId] ?: UUID.fromString("")

        return PickTicket(
                UUID.randomUUID(),
                getLocationFromId(supplyingLocationId),
                getLocationFromId(requestingLocationId),
                getRandomNote(),
                getRandomCaseNumber(),
                buildRequisitionLines(requisitionPars.toList())
        )
    }

    fun getParsFromRandomSupplyingLocation(): List<UUID> {
        var parsAtSupplyingLocation: List<UUID>
        do {
            // get a random supplying location
            val supplyingLocationId: UUID = parsBySupplyingLocation.keys.elementAt((SecureRandom().nextInt(parsBySupplyingLocation.size)))
            log.debug("supplyingLocationId: " + supplyingLocationId)
            // get pars for this location
            parsAtSupplyingLocation = parsBySupplyingLocation[supplyingLocationId] ?: emptyList()
            log.debug("parsAtSupplyingLocation: " + parsAtSupplyingLocation)
        } while (parsAtSupplyingLocation.isEmpty())
        return parsAtSupplyingLocation
    }

    fun getParsFromRandomRequestingLocation(): List<UUID> {
        var parsAtRequestingLocation: List<UUID>
        do {
            // get a random requesting location
            val requestingLocationId: UUID = parsByRequestingLocation.keys.elementAt((SecureRandom().nextInt(parsByRequestingLocation.size)))
            log.debug("requestingLocationId: " + requestingLocationId)
            // get pars for this location
            parsAtRequestingLocation = parsByRequestingLocation[requestingLocationId] ?: emptyList()
            log.debug("parsAtRequestingLocation: " + parsAtRequestingLocation)
        } while (parsAtRequestingLocation.isEmpty())
        return parsAtRequestingLocation
    }

    fun buildCache() {
        log.debug("Reading generator data files")

        // base file dir
        val fileDir = "generatorData"

        // Locations
        var fileName = "locations"
        var resource = ClassPathResource(fileDir + "/" + fileName)
        var resourceInputStream = resource.inputStream

        resourceInputStream.bufferedReader().useLines {
            lines ->
            lines.drop(1).forEach { line ->
                val myLine: Array<String> = line.split(",".toRegex()).toTypedArray()
                val loc = Location(UUID.fromString(myLine[0]), myLine[1], myLine[2].toBoolean())
                locations.add(loc)
                locationsById.put(UUID.fromString(myLine[0]), loc)
            }
        }

        // Notes
        fileName = "notes"
        resource = ClassPathResource(fileDir + "/" + fileName)
        resourceInputStream = resource.inputStream

        resourceInputStream.bufferedReader().useLines { lines -> lines.drop(1).forEach { notes.add(it) } }

        // Pars
        fileName = "pars"
        resource = ClassPathResource(fileDir + "/" + fileName)
        resourceInputStream = resource.inputStream

        resourceInputStream.bufferedReader().useLines {
            lines ->
            lines.drop(1).forEach { line ->
                val myLine: Array<String> = line.split(",".toRegex()).toTypedArray()
                requestingLocationByPar.put(UUID.fromString(myLine[2]), UUID.fromString(myLine[0]))
                supplyingLocationByPar.put(UUID.fromString(myLine[2]), UUID.fromString(myLine[1]))
            }
        }

        parsByRequestingLocation = requestingLocationByPar.toList().groupBy({ it.second }, { it.first })
        parsBySupplyingLocation = supplyingLocationByPar.toList().groupBy({ it.second }, { it.first })
    }

    fun getLocationFromId(locationId: UUID): Location {
        return locationsById[locationId] ?: throw IllegalArgumentException("supplying location expected")
    }

    fun buildRequisitionLines(pars: List<UUID>): List<RequisitionLine> {
        val requisitonLines: MutableList<RequisitionLine> = mutableListOf()
        pars.mapTo(requisitonLines) { RequisitionLine(UUID.randomUUID(), SecureRandom().nextInt(20).toString(), it) }
        return requisitonLines
    }

    fun getRandomNote(): String {
        return notes[SecureRandom().nextInt(notes.size)]
    }

    fun getRandomCaseNumber(): String {
        return SecureRandom().nextInt(99999).toString()
    }

    fun isSquareBracket(c: Char): Boolean {
        return c == '[' || c == ']'
    }

    fun locationToStringArray(location: Location): Array<String> {
        return listOf(location.id.toString(), location.name, booleanToTinyInt(location.isSupplying).toString()).toTypedArray()
    }

    fun booleanToTinyInt(boolean: Boolean): Short {
        return if (boolean) 1
        else 0
    }

    fun getReadOnly(list: List<Any>): List<Any> {
        return Collections.unmodifiableList(list)
    }

}
